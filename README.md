# Whiteboarding And Algorithms Workshop - Wednesday Edition

Because jobhunting and preparing for job interviews by yourself sucks.

It's better to do it with a community! Come to [Noisebridge](https://www.noisebridge.net) and practice whiteboarding, algorithms, and other interviewing techniques together with others!! =D (This is also a support group for job-hunters.)

This event will be in the Hackitorium, and will be self-organized / self-directed!! ^_^

All experience levels are welcome.

(Arrive any time between the start & finish times, at your own convenience.  There's no strict start & end time.)

(For newcomers, this is how you get in: to go inside, please ring the doorbell.)

~~~~~~~~~~~~~~~~~~~~~~~~~~~~
This workshop was inspired by "Whiteboarding Wednesdays" at Dev Bootcamp.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## My Personal Opinion About Whiteboarding Interviews:
![Useless Whiteboard Interviews: How to make the hiring process as arbitrary and subject to bias as possible.](https://pbs.twimg.com/media/CfNJTAHWAAAik5I.png)

https://twitter.com/ThePracticalDev/status/716997357624287232/photo/1

https://twitter.com/mirror2mask/status/790779116395962368

https://pbs.twimg.com/media/CfNJTAHWAAAik5I.png

https://medium.freecodecamp.org/why-is-hiring-broken-it-starts-at-the-whiteboard-34b088e5a5db

But unfortunately, this is the world that we live in.

## My personal tips to get good at interviews:
There are actually 2 different skills being tested in interviews, especially whiteboarding interviews. The first skill is public speaking / communication skills, and the second skill is technical proficiency. Both of these skills need to be practiced in tandem, and are equally important.

These are what you need to do:
1. Practice public speaking / communication skills
  - Get used to speaking in front of others and narrating your thoughts. Going to whiteboaring interview practice gets you used to talking in front of other people, so instead of associating the emotions of nervousness and anxiousness with public speaking, you associate public speaking with familiarity.

2. Know your stuff
  - Be very comfortable and familiar with your skills, because it's hard enough to do public speaking, while trying to remember the technical parts.
  - Good way to get better at programming is to work on something you want to make, or work on something that you really care about.  Example, I love videogames, and I want to make videogames, so that's how I got better at programming.

## List Of Resources:

#### "Structure And Interpretation Of Computer Programs"
http://web.mit.edu/alexmv/6.037/sicp.pdf

#### "Algorithmic Puzzles"
https://www.youtube.com/watch?v=Gv9ezWBy4yI

https://doc.lagout.org/science/0_Computer%20Science/2_Algorithms/Algorithmic%20Puzzles%20%5BLevitin%20%26%20Levitin%202011-10-14%5D.pdf

https://www.slideshare.net/amrinderarora/algorithmic-puzzles

#### Advanced Algorithms
https://www.youtube.com/watch?v=0JUN9aDxVmI

#### Websites That I Like To Use For Practice Questions:
1. https://projecteuler.net/ << my favorite, has math problems
2. https://www.codingame.com/ << my other favorite
3. https://www.codewars.com/
4. http://www.spoj.com/
5. https://leetcode.com/
6. https://triplebyte.com/
7. https://www.hackerrank.com/ << it's ok, except that I don't like the variable names that they use.

#### "Women Who Code" Meetup group's resources

https://github.com/WomenWhoCode/Algorithms-InterviewPrep

https://github.com/WomenWhoCode/Algorithms-InterviewPrep/blob/master/Resources.md

http://meetupresources.herokuapp.com/index.html

http://kelukelu.me/interview/

http://kelukelu.me/interview/questions.html

#### Awesome Algorithms
https://github.com/tayllan/awesome-algorithms

#### https://github.com/Robotikus/algorithm-interview

#### Interview Cake Questions (no answers)
https://www.interviewcake.com/all-questions/python
(You can also subscribe your email to get weekly questions.)

#### Cracking The Code Interview
https://github.com/alexitaylor/Cracking-the-Coding-Interview-6th-Edition

#### I personally have not tried these, but you should check these out:

https://medium.freecodecamp.org/the-10-most-popular-coding-challenge-websites-of-2016-fb8a5672d22f

##### The Awesome Lists:
Curated list of awesome lists: https://github.com/sindresorhus/awesome

A curated list of awesome awesomeness: https://github.com/bayandin/awesome-awesomeness

https://github.com/tayllan/awesome-algorithms

https://github.com/jnv/lists

## If you want more community-oriented events and other cool stuff like this, please donate to Noisebridge:
https://donate.noisebridge.net/

or

https://www.patreon.com/noisebridge

## Join the community! \^_^
https://www.discord.gg/JnfB58Z